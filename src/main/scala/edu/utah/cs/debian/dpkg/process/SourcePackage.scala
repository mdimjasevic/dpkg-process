/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import Builder._
import java.net.URI
import scala.io.Source
import scala.util.matching.Regex

/**
 * A class for managing a Debian source package.
 *
 * @constructor Create a new source package from a URI path to its .dsc file
 * @param path The path to a .dsc file
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
class SourcePackage(path: URI) {

  /**
   * @constructor Creates a new source package from a string path to its
   *              .dsc file
   */
  def this(pathStr: String) =
    this(new java.io.File(pathStr).toURI)

  /** Reads in the contents of this source package. */
  def content: String = Source.fromURI(path).mkString

  /**
   * Checks if this source package depends on debhelper (>= 9).
   *
   * @return true if the package depends on debhelper version at least 9,
   *         false otherwise
   */
  def isDH9(): Boolean =
    !"(\\r?\\n) ".r
      .replaceAllIn(content, " ")
      .mkString
      .split("\\r?\\n")
      .filter(_.startsWith("Build-Depends:"))
      .filter("""debhelper \(>= 9.*\)""".r.findFirstIn(_).isDefined)
      .isEmpty

  /**
   * Builds this source package.
   *
   * @param builder A builder to use (either pbuilder or cowbuilder)
   * @param privileged A flag indicating if the builder should be executed
   *        with privileged permissions
   * @return 0 on success, non-zero otherwise
   */
  def build(builder: Builder, privileged: Boolean = true): Int =
    BuilderCommand.run(builder,
      "--build " + path.toString.replace("file:", ""),
      privileged)

  /**
   * Executes a script on this source package.
   *
   * @param builder A builder to use (either pbuilder or cowbuilder)
   * @param scriptAndOptions The full path to a script and its options to
   *        execute
   * @param privileged A flag indicating if the builder should be executed
   *        with privileged permissions
   * @return 0 on success, non-zero otherwise
   */
  def execute(builder: Builder,
              scriptAndOptions: String,
              privileged: Boolean = true): Int =
    BuilderCommand.run(builder,
      "--execute -- " + scriptAndOptions,
      privileged)

  /** Returns this object. */
  def apply(): SourcePackage = this
}
