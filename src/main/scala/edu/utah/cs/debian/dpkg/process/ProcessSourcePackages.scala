/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import Builder._
import java.io.File
import scala.collection.parallel._

/**
 * Processes source packages in ways provided by the SourcePackage class.
 *
 * @constructor Create a new source package processing instance with a
 *              directory and a thread pool size
 * @param rootDir A root directory where source packages can be found
 * @param jobs An integer specifying the number of jobs to run in parallel
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
class ProcessSourcePackages(rootDir: File, jobs: Int = 1) {
  require(rootDir.isDirectory())
  require(jobs > 0)

  // source package .dsc files in the specified directory
  def sourceFiles: ParSeq[File] = FindFiles.allSourcePackages(rootDir).par

  /**
   * Runs the function on all source packages in parallel.
   *
   * @param function A function from [[SourcePackage]] to be executed
   * @param builder A builder to use (either pbuilder or cowbuilder)
   * @param privileged A flag indicating if the builder should be executed
   * 	       with privileged permissions
   * @return A parallel sequence of integers, where an integer is 0 if the
   *         function succeeded on a source package, and non-zero otherwise
   */
  def run(function: (SourcePackage, Builder, Boolean) => Int,
          builder: Builder, privileged: Boolean): ParSeq[Int] = {
    // initialize the build environment
    new BuildEnvironment(builder, reset = true)
    // process all source packages
    sourceFiles.tasksupport =
      new ForkJoinTaskSupport(
        new scala.concurrent.forkjoin.ForkJoinPool(jobs))
    sourceFiles map {
      f => function(new SourcePackage(f.toURI), builder, privileged)
    }
  }

  /**
   * Runs the function on all source packages in parallel with cowbuilder.
   *
   * @param function A function from [[SourcePackage]] to be executed
   * @return A parallel sequence of integers, where an integer is 0 if the
   *         function succeeded on a source package, and non-zero otherwise
   */
  def run(function: (SourcePackage, Builder, Boolean) => Int): ParSeq[Int] =
    run(function, cowbuilder, true)
}
