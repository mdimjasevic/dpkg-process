/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import Builder._
import sys.process._

/**
 * Sets up a package build environment.
 *
 * @constructor Create a new build environment with a builder, possibly
 * cleaning up artifacts from before, and a privileged (sudo) execution
 * mode.
 * @param builder A builder to use (either pbuilder or cowbuilder)
 * @param reset A flag indicating whether the build environment should be
 *        cleaned up first
 * @param privileged A flag indicating if the builder should be executed
 *        with privileged permissions
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
class BuildEnvironment(builder: Builder, reset: Boolean = false,
                       privileged: Boolean = true) {

  val debianDistribution = "stretch"
  val debianMirror = "http://debian.usu.edu/debian/"
  val buildPlace = "/var/cache/pbuilder/base.cow"

  if (reset) {
    clean()
    create()
  } else
    update()

  /**
   * Cleans up any prior build images.
   *
   * @return 0 on success, non-zero otherwise
   */
  def clean(): Int = {
    val exitCode = BuilderCommand.run(pbuilder, "--clean", privileged)
    (if (privileged) "sudo " else "") + "rm -rf " + buildPlace !

    exitCode
  }

  /**
   * Creates a new build image.
   *
   * @return 0 on success, non-zero otherwise
   */
  def create(): Int =
    BuilderCommand.run(builder,
      "--create" +
        " --distribution " + debianDistribution +
        " --mirror " + debianMirror,
      privileged)

  /**
   * Updates a build image.
   *
   * @return 0 on success, non-zero otherwise
   */
  def update(): Int =
    BuilderCommand.run(builder,
      "--update", privileged)
}
