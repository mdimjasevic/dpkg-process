/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import Builder._
import sys.process._

/**
 * Runs a builder command. Supported builders are given in the Builder
 * enumeration.
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
object BuilderCommand {

  /**
   * Runs a builder command constructed from the builder name, its arguments
   * and a privileged permissions flag.
   *
   * @param builder A builder to use (either pbuilder or cowbuilder)
   * @param cmdAndArgs A command for the builder and its arguments
   * @param privileged A flag indicating if the builder should be executed
   *        with privileged permissions
   * @return 0 on success, non-zero otherwise
   */
  def run(builder: Builder, cmdAndArgs: String,
          privileged: Boolean = true): Int =
    (if (privileged) "sudo " else "") + builder + " " + cmdAndArgs !
}
