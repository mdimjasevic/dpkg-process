/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process.cli

import org.apache.commons.cli.{
  DefaultParser,
  CommandLine,
  Options,
  Option => CLIOption
}
import scala.util.Try

/**
 * A command line argument parser.
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
object Parser {

  /** Command line options used with the application */
  val options: Options =
    Set(
      new CLIOption(
        "f",
        "find",
        true,
        "Find source packages"),
      new CLIOption(
        "9",
        "isdh9",
        true,
        "Check if a source package depends on debhelper >= 9"),
      new CLIOption(
        "b",
        "build",
        true,
        "Build source packages"),
      new CLIOption(
        "e",
        "execute",
        true,
        "Execute a scirpt on source packages"),
      new CLIOption(
        "j",
        "jobs",
        true,
        "Number of jobs used in building source packages " +
          "(use it with the build argument)"),
      new CLIOption(
        "s",
        "script",
        true,
        "The full path to a script and its options " +
          "(use it with the execute argument)"),
      new CLIOption(
        "h",
        "help",
        false,
        "Print this message"),
      new CLIOption(
        "v",
        "version",
        false,
        "Print the version of the program"))
      .foldLeft(new Options)(_.addOption(_))

  /**
   * Parses command line arguments.
   *
   * @param args Arguments provided by the user
   * @return A CommandLine object corresponding to the provided arguments,
   *         wrapped with the Try type
   */
  def parse(args: Array[String]): Try[CommandLine] =
    Try(new DefaultParser().parse(options, args))
}
