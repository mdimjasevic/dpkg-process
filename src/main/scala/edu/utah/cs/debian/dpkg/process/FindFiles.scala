/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import java.io.File
import scala.util.matching.Regex

/**
 * A utility class for finding files.
 *
 *  @author Marko Dimjašević <marko@cs.utah.edu>
 */
object FindFiles {

  /**
   * Recursively searches all files matching a regular expression.
   *
   * @param f A root directory where the search should start
   * @param r A regular expression matching names of files to be returned
   * @return A stream of files found by the search
   */
  def recursiveListFiles(f: File, r: Regex): Stream[File] = {
    require(f.isDirectory())

    val currentDirFiles = f.listFiles.toStream
    val matching = currentDirFiles.filter(
      f => r.findFirstIn(f.getName).isDefined)
      .toStream
    matching append currentDirFiles.filter(_.isDirectory)
      .flatMap(recursiveListFiles(_, r))
  }

  /**
   * Finds all source packages in a directory.
   *
   * @param f A root directory where the search should start
   * @return A stream of source package files found by the search
   */
  def allSourcePackages(f: File): Stream[File] =
    recursiveListFiles(f, new Regex(""".*\.dsc$"""))
}
