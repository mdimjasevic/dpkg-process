/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import Builder._
import com.typesafe.config.{ Config, ConfigFactory }
import edu.utah.cs.debian.dpkg.process.cli.Parser
import java.io.File
import org.apache.commons.cli._
import scala.util.{ Try, Success, Failure }

/**
 * 	The entry-point object of dpkg-process.
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
object Main extends App {

  // read in the application.properties resource
  val conf = ConfigFactory.load
  val appName = conf.getString("application.name")
  val appVersion = conf.getString("application.version")

  /** Checks for command line options and executes them. */
  private object cli {

    /**
     * Prints the program's help and an error message and exits.
     *
     * @param errMsg An error message
     */
    def printHelp(errMsg: String): Unit = {
      if (errMsg.length > 0) println(errMsg)
      new HelpFormatter().printHelp(appName, Parser.options)
    }

    /** Prints the program's help and exits. */
    def printHelp(): Unit = printHelp("")

    /**
     * Checks if there is a conflict between options and executes them if
     * there is no conflict.
     *
     * @param line A line with command line options provided by the user
     */
    def executeOptions(line: CommandLine): Unit = {

      /**
       * Finds source packages in the directory specified on the command
       * line with the -f parameter.
       */
      def runFindCmd(): Unit =
        FindFiles.allSourcePackages(new File(line.getOptionValue('f')))
          .map { println }

      /**
       * Checks if the source packages build-depend on dh >= 9 specified
       * on the command line with the -9 parameter.
       */
      def runIsDH9Cmd(): Unit =
        println(new SourcePackage(line.getOptionValue('9')).isDH9)

      /**
       * Runs a builder with the specified function operation.
       *
       * @param rootDir A root directory with source packages
       * @param f A function to execute on each package
       *
       * @return A sequence of integers, each integer indicating whether
       *         the function finished successfully (0) or not (non-zero).
       */
      def runBuilderCmd(
        rootDir: String,
        f: Function3[SourcePackage, Builder, Boolean, Int]) = {
        // this needs error handling in case the argument is not an integer
        val jobs =
          if (line.hasOption('j')) line.getOptionValue('j').toInt else 1

        val proc = new ProcessSourcePackages(new File(rootDir), jobs)
        proc.run(f)
      }

      /**
       * Runs builders to build source packages from a root directory
       * specified on the command line with the -b parameter.
       */
      def runBuildCmd(): Unit = {
        val buildFunction: Function3[SourcePackage, Builder, Boolean, Int] =
          _.build(_, _)
        runBuilderCmd(line.getOptionValue("build"), buildFunction)
      }

      /**
       * Runs builder to execute a custom script specified with the
       * -s parameter on source packages from a root directory specified with
       * the -e parameter.
       */
      def runExecuteCmd(): Unit = {
        if (!line.hasOption('s'))
          printHelp("The execute argument must be used in " +
            "conjunction with the script argument")
        else {
          val execFunction: Function3[SourcePackage, Builder, Boolean, Int] =
            _.execute(_, line.getOptionValue('s'), _)
          runBuilderCmd(line.getOptionValue("execute"), execFunction)
        }
      }

      /** Prints the program's name and version. */
      def printVersion(): Unit =
        println(s"${appName} ${appVersion}")

      // TODO: all these options need error checking
      val optionAndAction: Map[Char, Function0[Unit]] = Map(
        'f' -> runFindCmd,
        '9' -> runIsDH9Cmd,
        'b' -> runBuildCmd,
        'e' -> runExecuteCmd,
        'v' -> printVersion,
        'h' -> printHelp)

      val numOfOptions = optionAndAction.keySet.foldLeft(0)(
        (count, option) => if (line.hasOption(option)) count + 1 else count)

      if (numOfOptions > 1)
        printHelp("Uncombinable options!\n")
      else if (numOfOptions == 0)
        printHelp()
      else
        // At this point we know only one option from optionAndAction is
        // present
        optionAndAction map {
          kv => if (line.hasOption(kv._1)) kv._2.apply
        }
    }
  }

  Parser.parse(args) match {
    case Success(line) => cli.executeOptions(line)
    case Failure(e)    => cli.printHelp(e.getMessage)
  }
}
