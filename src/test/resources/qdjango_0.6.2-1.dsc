-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: qdjango
Binary: libqdjango-db0, libqdjango-http0, libqdjango-dbg, libqdjango-dev, libqdjango-doc
Architecture: any all
Version: 0.6.2-1
Maintainer: Jeremy Lainé <jeremy.laine@m4x.org>
Homepage: https://github.com/jlaine/qdjango
Standards-Version: 3.9.6
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=collab-maint/qdjango.git
Vcs-Git: git://anonscm.debian.org/collab-maint/qdjango.git
Build-Depends: debhelper (>= 9), qtbase5-dev, libqt5sql5-sqlite
Build-Depends-Indep: doxygen
Package-List:
 libqdjango-db0 deb libs optional arch=any
 libqdjango-dbg deb debug extra arch=any
 libqdjango-dev deb libdevel optional arch=any
 libqdjango-doc deb doc optional arch=all
 libqdjango-http0 deb libs optional arch=any
Checksums-Sha1:
 6e38c68a80a9706d2cb68398146ec672a9bbeb3d 98685 qdjango_0.6.2.orig.tar.gz
 b5798f0549f558a0206166524202c9ba044099e1 3276 qdjango_0.6.2-1.debian.tar.xz
Checksums-Sha256:
 a78630d677255ea144c0db2b89dba1c96c99c281f66968e2879a9c2ffee89bc7 98685 qdjango_0.6.2.orig.tar.gz
 4639964aa55160961ed50ff0f164c939a00f8384eabb9c9941dc12babd0a6f3d 3276 qdjango_0.6.2-1.debian.tar.xz
Files:
 8afe03390c969759e0007429f19d0de9 98685 qdjango_0.6.2.orig.tar.gz
 557c2c2b26f680348ef557fe489ad161 3276 qdjango_0.6.2-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCAAGBQJWKK6NAAoJENLPZJIaziaHkKkQALuf0mEKTRmgbPkPJb/pPdhl
ou2TGjjAXcJYd8kzryXUKhEPsi6zvFoRdeISfyDbguuzAKHA/wy4xhXt5jOfjVTQ
ptUHMKfPDhmNpr4oSyBG5/KVISUzJgiQs5WlF5Yy2ydvXpO44rBPuULMLdFqBxk7
gDaJ9Z+Pj8SjXMp0MGfGgl5gqo+8TuWm9Vohmx2cJRwRxHHfJRPTgUpR1bsOQQGm
X8ftkWJoMnyw2yZ6La+ciwuX0MaVTdh+8MoTOqPHXdFmnGhgm3fZ4QAwy8HwutVc
lhLv0Sm0CeRlbg83ooCqnf1czPd+5USMMKmOvaTDyfJynq5k/sENB3/6jZHSuPWj
Xw4174YcFgNbvmIt9p0vbyZfDWNwBR0IqcoYn4tboP5AkUMHNHq48jOcYYAspOoz
83JmmO4/z/TDoSNqjFXTR5vKRPb21Gtx0SYEXMyJWinXljAH0YNTdKhMMKkKg9mG
pvEe6nsVlzkorvuY63SCnaB4J5jUsTifpZakXW94MQIg51m29kjWiXeE1mGr3nXs
VLUiebWAOuokZsdVQ4k7ktAB0xEXFntHW6ZifL4lolqoOS6Uwjt0KlQYSbthvFqC
8kwGHy6bJbH+65Sp7XUi5ElMquQ8Oeq/pVDFDu5abFIqQ5pJVOD5KLgLNcVXn7Ay
02ofsPPulMLjnpTOHAgK
=oAwk
-----END PGP SIGNATURE-----
