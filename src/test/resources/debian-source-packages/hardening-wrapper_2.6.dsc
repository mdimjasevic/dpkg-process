-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: hardening-wrapper
Binary: hardening-wrapper, hardening-includes
Architecture: any all
Version: 2.6
Maintainer: Package Hardening <hardening-discuss@lists.alioth.debian.org>
Uploaders: Kees Cook <kees@debian.org>, Moritz Muehlenhoff <jmm@debian.org>
Homepage: http://wiki.debian.org/Hardening
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/loggerhead/hardening/master/files/head:/hardening-wrapper/
Vcs-Bzr: http://anonscm.debian.org/bzr/hardening/master
Build-Depends: debhelper (>= 9), perl-base (>= 5.10)
Package-List:
 hardening-includes deb devel optional arch=all
 hardening-wrapper deb devel optional arch=any
Checksums-Sha1:
 25be7ce2fc5ec1359c56050549b62e8e691d7fc0 19436 hardening-wrapper_2.6.tar.xz
Checksums-Sha256:
 c5fc46439646d0929a0605e4f3db67e57eefbbf5ceec5a2888440dbdf4450224 19436 hardening-wrapper_2.6.tar.xz
Files:
 47c93c05b4d0199be8df0d35dbd68192 19436 hardening-wrapper_2.6.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1
Comment: Kees Cook <kees@outflux.net>

iQIcBAEBCgAGBQJUIyXWAAoJEIly9N/cbcAm7L4P/i62pu2Ll5k+ngAZTpsKzJow
MgZMATNvgH6tAGe2hQ41nB+HbeAZHhkK0rkbPQ5yZb8OHsqIX4yEibGTjmu9CPJm
LEHondXAAwmqxZZA557BAj8GLRqD9prHyUePGoqYzk93jNDkB1gDSqD2hoyNvqGi
t78/+avRzOGPRrQJ/UaHZcEeoNEW0cpaF7K6TksgrZgU7sQb6BcbFmaXxGcr3q3F
zC9TK7qGWALf8oKkhmoaMTfRXB61qBpHqiunZ27aVXIKLs4I3ff0z3sQOstxhffx
FHC/x/Too/GMqL7kHPtGJXaYgkfVooBdnOMHpDZFAaS8lUAWennwrNpD+ChP52p+
7xnKxXY2YZcJvqdSpliVFDyrw4thfVOIjGWryYjIaXXA5YY+F83KqYo1+Cj5iiar
rUV45JKXqt4pIddyBoQ8MYIvY9LcEg/1As0amiNEtXz3xwpnKEYcpUR6Ro/0LzJe
sW5XMT042lj/EfZ2hrBpK3NWcr7uRDCGyOwDgS/pjZaV5OqJCiAZBlmJrigpgxr1
O8LoNuhqtL8Ihla7nObiGIZDa+mXMs5cyA8xJGY2zuXr8HQTH6TatYW63P1th6CH
MqO3RhQKer5ij1reFmOUw9wUn6tpCMiATTUWr7jrMvD9B/d7mQcSLHvb4Yk7xMNK
P5GHHmzCxNx8osJR1EFN
=HRzu
-----END PGP SIGNATURE-----
