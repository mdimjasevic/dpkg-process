-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 1.0
Source: hostname
Binary: hostname
Architecture: any
Version: 3.17
Maintainer: Debian Hostname Team <hostname-devel@lists.alioth.debian.org>
Uploaders: Michael Meskes <meskes@debian.org>
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 5)
Package-List:
 hostname deb admin required arch=any essential=yes
Checksums-Sha1:
 844349103a0e51c71880946848585f8e86a368a9 13640 hostname_3.17.tar.gz
Checksums-Sha256:
 34acec3481861e2fe6d54550d427f692c6e3a5daf8dccbf1407b6e6a8130043b 13640 hostname_3.17.tar.gz
Files:
 9a019b8ee8c1e27fe1ef700afcea01a1 13640 hostname_3.17.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIVAwUBVscFnTXYV8C7yzslAQjROQ/9E04lt0lWe4B2CEOE4CaqoT1aGnXY5cMB
+4PgpRVAA6QMB6QI7c2bdcb2927ogavb/yLwJpxD2N4W6xGwvk4cxcv8QqrtpetA
bcaUPII0AlNv9DbcfLlux34mTrGaPClQhC21k4HByLTtfCGn3JjbdL3NWP2XPgx2
JhOOswOcVaDuzOCY5P5K9AbnNDJzikK8s3uGasQ4B5UwpRuWdDYAU7zbnqBJ6D0n
yd/t3dRIeZY0ht1u8JmdzM8USd4d9sqt1UVJWXCXc5pDkbuYmHPQYq4Vu3+QnaN0
j3NU2AAoGyXqcPCt++YOMH900xsAmBfCMtRCu13iBrlJMwRN61ZJT43vdVcFZQGt
X8ZdfQe3EWZp7j9XInHo9aO4x622Ik1bU9rF3HTNuJnP2javCrezzHruqNlWZwmX
JJe2VbkYfDPwMU4ttaT01NISRxSuFOINLaADFhzcajygbb2hAy7K9BtSBT3EA4Jn
DsBJofkOLI/632SVn6qKbKEI6iRMB/whg0fIHweTgX2ElgIbYpoNl5u9kJI0Mfyk
JXqmpB2pJyaHZadzxdjUFbwvgYeZz1gJ2++ltcA59WX71Ca43eOt2Hb5PKBx6AL7
6CmtM68dbU5sDPkruFhLeLnWQKCp6j9gzmfZrJL/hWUlXKdEyvuphm3znzwZRDnJ
JpCBGw6Hc9M=
=v114
-----END PGP SIGNATURE-----
