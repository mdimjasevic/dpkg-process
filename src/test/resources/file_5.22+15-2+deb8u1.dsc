-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: file
Binary: file, file-dbg, libmagic1, libmagic-dev, python-magic, python3-magic
Architecture: any all
Version: 1:5.22+15-2+deb8u1
Maintainer: Christoph Biedl <debian.axhn@manchmal.in-ulm.de>
Homepage: http://www.darwinsys.com/file/
Standards-Version: 3.9.5
Build-Depends: debhelper (>= 9.20141010~), dh-autoreconf, python-all, python3-all, zlib1g-dev
Package-List:
 file deb utils standard arch=any
 file-dbg deb debug extra arch=any
 libmagic-dev deb libdevel optional arch=any
 libmagic1 deb libs standard arch=any
 python-magic deb python optional arch=all profile=!stage1
 python3-magic deb python optional arch=all profile=!stage1
Checksums-Sha1:
 fa2ad4a9e78aa2b536d294da5c107f2c47597723 569332 file_5.22+15.orig.tar.xz
 a71a8ddbb98a3305c40316dcc6dbaf88ba3d7458 29844 file_5.22+15-2+deb8u1.debian.tar.xz
Checksums-Sha256:
 c021e9f73b3eb3b6cc2532c5d9a77af1a92902935013c2740ba3fef83f1804d2 569332 file_5.22+15.orig.tar.xz
 01e040a31271639aa39f064752b214dfa1b48da65d5b73cb5e2e74f60f830a26 29844 file_5.22+15-2+deb8u1.debian.tar.xz
Files:
 c07e124bf217ade299453ec2a2dd91da 569332 file_5.22+15.orig.tar.xz
 3fc8b04debdcdeba5ddd748cc75e42ff 29844 file_5.22+15-2+deb8u1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCgAGBQJWGYf7AAoJEMQsWOtZFJL9A9wQALWg1YS8RXSP5Ojh5Hfa4ZZ+
QFjcH44cgNumiM7yJcBPFAmhBbFx9wykr8Fev/SZXbXP6SFLHeLHhdHq0VAF6lmt
mIElRMTOSaoXSxxURkb6h4Jo37WOjUF+IASkZ0/E78nsR1zGRD60WlUOv6O6Oz3K
dj06/ddfoVxBfv8Mny/Gqdb+JICTkkDhlu6RLOPmARdB1Ks8HzWRijuU2/BNtCBn
iTvg4lPO+vn7UTScbj8mH9Qo6x9Bk87JEBVC4VCu7GkTYBoUWEosWvWC/A8v1MVx
xD3HYa+STOdoWUNSseYKWbEC61xn44XH/grYCIzVunpfqgS95n8GUczEns3LKCgG
TATebn6vHmRmR6dnQ1+QtK5CySJw7SicEIfo6DroS6ONS5zid7hwOQe2r3et/QoM
mJe7QKrLAGQurl/GV9uSk6U9cYqqcA4QRpnbRzqvbCv5WOIu0/mGNhvvsHWDFn2L
9W4+Cb8Qas+LZpjsocpIeHMq10xRCsq/MlwSL9qVdrv2MQO8SFgtGiWUB62XLcMo
y+EG2/9C38STD/2wQ7FzT6t/Yke0dLwgGoDVVZ+0Akpz4wEOZTKJNi98m9IiQnoN
BI7V8LbdY4R0U5LsK2bnOGunwY6RHNVWSNyAsnG8o2SsZJZ+d7TzIBQsjLNcJAa+
5ryTi9rtIsak3uvBq5Kw
=ovXv
-----END PGP SIGNATURE-----
