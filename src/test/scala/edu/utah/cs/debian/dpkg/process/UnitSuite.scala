/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import java.io.File
import scala.util.matching.Regex

/**
  * Unit testing.
  * 
  * @author Marko Dimjašević <marko@cs.utah.edu>
  */
class UnitSuite extends UnitSpec with TestConfig {
  
  val descriptionTestFiles = Map(
    "/abc.dsc" -> false,
    "/debhelper-old-1.dsc" -> false,
    "/debhelper-old-2.dsc" -> false,
    "/dh9-1.dsc" -> true,
    "/dh9-2.dsc" -> true,
    "/file_5.22+15-2+deb8u1.dsc" -> true,
    "/qdjango_0.6.2-1.dsc" -> true, // this is a Unicode file
    sourcePackageDir + "/hostname_3.17.dsc" -> false,
    sourcePackageDir + "/hardening-wrapper_2.6.dsc" -> true
  )
  
  property("A description file with debhelper (>= 9) under Build-Depends"
      + " should return true, and false otherwise") {
    
    descriptionTestFiles map {
        f => assert(new SourcePackage(getClass.getResource(f._1).toURI).isDH9()
            == f._2) }
  }
  
  property("There should be exactly " + descriptionTestFiles.keySet.size
      + " expected .dsc files in the resource directory") {
    
    val resourceDir = new File(getClass.getResource(
      descriptionTestFiles.keySet
      .filter(f => !f.startsWith(sourcePackageDir))
      .toStream(0))
      .toURI)
      .getParentFile
    val actualSourceFiles = FindFiles.allSourcePackages(resourceDir)
      .map(_.getName)
      .toSet
    val expectedSourceFiles = descriptionTestFiles.keySet
      .map(f => ("/(" + sourcePackageDir.substring(1) + "/)?")
          .r
          .replaceFirstIn(f, ""))
    // check these two sets are equal (and of size 
    //   descriptionTestFiles.keySet.size)
    assert((actualSourceFiles subsetOf expectedSourceFiles)
        && (expectedSourceFiles subsetOf actualSourceFiles))
  }
}
