/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

import Builder._
import java.io.File
import sys.process._

/**
 * Integration testing.
 *
 * @author Marko Dimjašević <marko@cs.utah.edu>
 */
class IntegrationSuite extends IntegrationSpec with TestConfig {

  info("As a package builder")
  info("I want to be able to build Debian packages from source")
  info("So I can analyze them in a number of ways")

  val hostnameDsc = sourcePackageDir + "/hostname_3.17.dsc"
  val builder = cowbuilder
  val cachedBuildResults = "/var/cache/pbuilder/result"
  val removeCmd = "sudo rm -f " + cachedBuildResults + hostnameDsc

  feature("Build package") {
    scenario("Building a package in a new build environment") {

      Given("there might be a build environment present")
      removeCmd !
      val buildEnv = new BuildEnvironment(builder, reset = true)

      When("the hostname package is built")
      val pkg = new SourcePackage(getClass.getResource(hostnameDsc).toURI)
      val r = pkg.build(builder)

      Then("the build process should return 0")
      assert(r == 0)
    }

    scenario("Building a package in an existing build environment") {

      Given("there is a build environment present")
      removeCmd !
      val buildEnv = new BuildEnvironment(builder, reset = false)

      When("the hostname package is built")
      val pkg = new SourcePackage(getClass.getResource(hostnameDsc).toURI)
      val r = pkg.build(builder)

      Then("the build process should return 0")
      assert(r == 0)
    }
  }

  feature("Process all source packages in a directory") {
    scenario("Building packages in parallel") {

      Given("a directory with source packages")
      val sourcePackageDir =
        new File(getClass.getResource(hostnameDsc).toURI)
          .getParentFile

      When("all the packages are built")
      val proc = new ProcessSourcePackages(sourcePackageDir, 2)
      val buildFunction: Function3[SourcePackage, Builder, Boolean, Int] =
        _.build(_, _)
      val buildReturnValues = proc.run(buildFunction)

      Then("all the build processes should finish successfully")
      buildReturnValues map { v => assert(v == 0) }
    }

    scenario("Executing a script on packages in parallel") {

      Given("a directory with source packages and a script")
      val sourcePackageDir =
        new File(getClass.getResource(hostnameDsc).toURI)
          .getParentFile
      val scriptAndOptions = "/bin/true"

      When("the script is executed on all the packages")
      val proc = new ProcessSourcePackages(sourcePackageDir, 1)
      val execFunction: Function3[SourcePackage, Builder, Boolean, Int] =
        _.execute(_, scriptAndOptions, _)
      val executionReturnValues = proc.run(execFunction)

      Then("all the script processes should finish successfully")
      executionReturnValues map { v => assert(v == 0) }
    }

  }

}
