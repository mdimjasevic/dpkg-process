/* Copyright 2016 Marko Dimjašević
 *
 * This file is part of dpkg-process.
 *
 * dpkg-process is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dpkg-process is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.
 */

package edu.utah.cs.debian.dpkg.process

/**
  * Shared configuration parameters between unit and integration testing.
  * 
  * @author Marko Dimjašević <marko@cs.utah.edu>
  */
trait TestConfig {
  
    /** A resource directory with source packages */
    val sourcePackageDir = "/debian-source-packages"
}