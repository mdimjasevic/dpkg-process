#!/bin/bash

# Copyright 2016 Marko Dimjašević
#
# This file is part of dpkg-process.
#
# dpkg-process is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dpkg-process is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.

set -e

tmp_dir=$(mktemp --directory)
apt_file=/etc/apt/sources.list

cp $apt_file $tmp_dir

sudo su -c "echo 'deb http://debian.usu.edu/debian/ jessie main' > $apt_file"
sudo su -c "echo 'deb http://mirrors.kernel.org/debian jessie-updates main' >> $apt_file"

sudo apt-get update
sudo apt-get install --yes maven cowbuilder

sudo su -c "echo 'deb http://debian.usu.edu/debian/ stretch main' > $apt_file"
sudo su -c "echo 'deb http://mirrors.kernel.org/debian stretch-updates main' >> $apt_file"

sudo apt-get update
sudo apt-get install --yes scala openjdk-8-jdk

sudo cp $tmp_dir/$(basename $apt_file) $apt_file
sudo apt-get update
rm -rf $tmp_dir

# Set Java 8 as default
sudo update-alternatives --set java  /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/java-8-openjdk-amd64/bin/javac

# Check Java and Java compiler versions
java -version
javac -version
