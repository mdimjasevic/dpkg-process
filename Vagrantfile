# coding: utf-8
# -*- mode: ruby -*-
# vi: set ft=ruby :

# Copyright 2016 Marko Dimjašević
#
# This file is part of dpkg-process.
#
# dpkg-process is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dpkg-process is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with dpkg-process.  If not, see <http: *www.gnu.org/licenses/>.

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
    config.cache.synced_folder_opts = {
      type: :nfs,
      mount_options: ['rw', 'vers=4', 'tcp', 'nolock']
    }
  end
  
  config.vm.box = "debian/jessie64"

  config.vm.provider :libvirt do |domain|
    domain.uri = 'qemu+unix:///system'
    domain.disk_bus = "virtio"
  end

  config.vm.synced_folder ".", "/home/vagrant/dpkg-process"

  config.vm.provision :shell, :privileged => false,
    path: "scripts/vagrant/deps.sh"
  config.vm.provision :shell, :privileged => false, inline: <<-SCRIPT
    cd dpkg-process
    mvn install
  SCRIPT
end
